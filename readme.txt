Proyecto "camUeye_CostaRica":
----------------------------- 

Software y versiones requeridas:
--------------------------------

OpenCV 2.1.0		(ya instalado en Pelican)
OpenCV 2.3.0
Ueye drivers 3.9	(ya instalado en Pelican, viene con el codigo)
Ueye drivers 4.0	(viene con el codigo)
GTK 2.0			(si no está disponible en el Pelican hay dos opciones: dejar de utilizarla [no aporta mucha funcionalidad] o instalarla)
Atlante 		(libreria publica del CVG, ya incluida con este software, viene con el codigo)
 
Opciones de configuracion:
--------------------------
	- En include/camUeye_defines.h, hay dos definiciones de constante que ayudan a entender como utilizar el driver de la camara:
		1) #define CAMUEYECAMERA_DEBUG
		Cuando esta constante está definida, se muestran por consola lineas que explican que esta haciendo el programa en cada momento. Basicamente se puede entender (1) cuando la camara esta adquiriendo imagenes, (2) cuando las imagenes estan siendo guardadas al disco duro, y (3) como funcionan las funciones publicas del driver de la camara (comparando con el codigo que hay en main.cpp).
		2) #define CAMUEYECAMERA_CAN_OPEN_WINDOW
		Esta opcion sirve para permitir (o no) que el driver pueda abrir una ventana mostrando la imagen de la camara. Cuando se vaya a ejecutar el driver en el Pelican durante un vuelo hay que dejar esta constante comentada (ya que consume recursos y el ordenador de a bordo es poco potente). Cuando prueben el programa en un ordenador de sobremesa o en un portatil pueden descomentar la opcion. Es util para comprobar que la camara funciona correctamente.
	- Usando el "ueyecameramamanager": se trata de un programa que aporta el fabricante de la camara para facilitar su configuracion.  Lo pueden descargar de internet de la siguiente pagina: http://www.ids-imaging.com/frontend/accessories.php?group=15&group2=42 , http://www.ids-imaging.com/drivers.php?cat=2 . Se arranca por consola ejecutando "ueyecameramanger". Lo mejor es que primero carguen los ficheros de configuracion que tienen en el proyecto "camUeye_CostaRica": "camUeye_CostaRica/bin/camera_RGB24_autoGain.ini" o "camUeye_CostaRica/bin/camera_RGB24.ini". En principio, lo unico que es necesario para que el driver funcione es que dejen la configuración de formato de la imagen (pestania Format) a: RGB24, Normal.	En el codigo se puede cambiar el fichero de configuración que utiliza el programa especificandolo en la sentencia my_camera.start( config_file, [...]).
		3) #define CAMUEYE_DRIVER_4p0
		Definir o quitar segun se utilice el driver 3.9 o el 4.0
		Y en el camUeye_CostaRica/bin/makefile hay que poner la linea correspondiente
		
		
Ejecutar el programa:
---------------------
	- Compilar el pograma ejecutando "make clean all" en la consola/terminal dentro de la carpeta camUeye_CostaRica/bin/
	- El programa se ejecuta introduciendo "./camUeyeJesusTest"
	
Explicacion de funciones basicas del driver:
--------------------------------------------

	[NOTA] 	Adquirir imagenes quiere decir que el driver esta tomando las imagenes de la camara, pero no las está guardando en el disco duro (puede enseñarlas en la ventana o no).
		Lo diferencio del guardado, donde las imagenes se adquieren y se guardan en la memoria SD del Pelican.

	- Camueye_Camera( cvgString window_name): constructor del objeto "camara", hay que especificar un nombre para la ventana que muestra las imagenes adquiridas. 
	- virtual ~Camueye_Camera(): destructor, se llama de manera automática.

	void start( cvgString config_filename, cvgString images_filename, float acq_period, bool log_images = false): prepara la camara para adquirir imagenes (para activiar la adquisicion continua [sin guardado] hay que llamar tambien a la funcion showImageWindow() ). Esta funcion tiene tres parametros 
	[cvgString config_filename] especificar fichero de configuracion para el driver de la camara (por ejemplo "camera_RGB24.ini", 
	[cvgString images_filename] fichero donde se guardaran las imagenes (si se pone "images/", estas serán guardadas en "camUeye_CostaRica/bin/images/", 
	[float acq_period] especifica cada cuanto tiempo se guarda una imagen en segundos (por ejemplo se puede especificar que se guarde 1 imagen cada 5.0 segundos), 
	[bool log_images] especifica si se comienzan a guardar imagenes de manera continua desde la llamada a la funcion start
	void stop(): parar la adquisicion de imagenes.
	

	void showImageWindow(): activa la adquisicion continua de imagenes (siempre llamar despues de start([...]) para que surta efecto), y las muestra en una ventana (si CAMUEYECAMERA_CAN_OPEN_WINDOW está definida)
	void hideImageWindow(): desactiva la adquisicion continua de imagenes (siempre llamar despues de start([...])
	void takeSnapshot():	guarda una imagen (siempre llamar despues de start([...])
	void activateLoggingImages():	activa el guardado continuo de imagenes
	void deactivateLoggingImages(): desactiva el guardado continuo de imagenes
	
Si tienen dudas, sobre como utilizar este programa no duden en contactarme, especialmente si hay errores en la compilación. He hecho lo posible porque no ocurran errores de compilacion, pero siempre puede haber algo que no haya previsto.
Un saludo,

Eng. Jesús Pestana Puerta
PhD Candidate & Researcher at
Computer Vision Group
CAR - Centre for Automation and Robotics
ETSII - Polytechnic University of Madrid
Jose Gutierrez Abascal 2
28006 Madrid
www.vision4uav.com

